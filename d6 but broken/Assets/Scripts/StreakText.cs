﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StreakText : MonoBehaviour
{

    public int currentCombo;
    public int currentScore;
    private int highestCombo = 0;
    private int highestScore;
    private int winStreak;
    Text combo;
    Text score;
    Text hitOrMiss;
    Text playerStats;
    GameObject scoreboard;
    GameObject announcement;
    GameObject statScreen;
    
    // Start is called before the first frame update
    void Start()
    {
        combo = GetComponent<Text>();
        scoreboard = GameObject.FindGameObjectWithTag("ScoreBoard");
        score = scoreboard.GetComponent<Text>();
        announcement = GameObject.FindGameObjectWithTag("WinLose");
        hitOrMiss = announcement.GetComponent<Text>();
        statScreen = GameObject.FindGameObjectWithTag("stats");
        playerStats = statScreen.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateStats();
        //Combo Field
        combo.text = "Combo: " +  +currentCombo;

        //Score Field
        score.text =  currentScore + "/50";


        //Win or Lose
        if (currentScore < 50)
        {
            hitOrMiss.text = "You Lose";
            winStreak = 0;
        }
        else { hitOrMiss.text = "Victory";
            winStreak++;
        }

        //Statistics
        playerStats.text =
            "Score \t" + currentScore + "\n" +
            "Biggest Combo \t" + highestCombo + "\n" +
            "High Score \t" + highestScore + "\n" +
            "Win Streak \t" + winStreak;





    }

    void UpdateStats() {
        //update the highest combo
        if (currentCombo > highestCombo) {
            highestCombo = currentCombo;
        }
        //update the highest score
        if (currentScore > highestScore)
        {
            highestScore = currentScore;
        }


    }
}
