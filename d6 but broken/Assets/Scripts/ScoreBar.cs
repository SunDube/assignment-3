﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreBar : MonoBehaviour
{

    //based very loosely https://www.youtube.com/watch?v=Gtw7VyuMdDc

    private Transform scoreBar;
    private Vector3 scale;
    float barScaleY = (float)0.2;
    float barScaleX = 0;
    private Vector3 tired;
    private int testScore;
    GameObject scores;


    

    // Start is called before the first frame update
    void Start()
    {
        scoreBar = transform.Find("score");
        tired = new Vector3(barScaleX, barScaleY);
        scores = GameObject.FindGameObjectWithTag("Combo");

    }

    // Update is called once per frame
    void Update()

    {
        testScore = scores.GetComponent<StreakText>().currentScore;
        tired = new Vector3(barScaleX, barScaleY);
        IncrementBar();
        scoreBar.localScale = tired;

        
    }

    void IncrementBar() {
        if (testScore <= 50) {

            barScaleX = (float)(testScore * 0.2);

        }

    }
}
