﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceInstantiator : MonoBehaviour
{
    //Store the 36 dice prefabs
    public GameObject[] dicePrefabs;

    //Maximum number of dice per row
    public int maxNumberDice;

    //The number of dice currently available in the coulumn
    public int diceCount;

    //time between dice spawns
    public float interval;


    // Start is called before the first frame update
    void Start()
    {
        //resets the dice count each time the program is run
        diceCount = 0;

        //makes sure that there is at least 1 dice when run or doesnt run at all
        if (maxNumberDice <= 0) return;

        //continues making dice
        InvokeRepeating("MakeSquare", interval, interval);

    }

    void MakeSquare() {

        //selects a random dice prefab
        GameObject dicePrefab = dicePrefabs[Random.Range(0, dicePrefabs.Length)];

        //creates the game object
        GameObject dice = Instantiate(dicePrefab, transform.position, Quaternion.identity);

        //increase the dice count
        diceCount++;

        //stop making dice once the maximum number of dice is reached
        if (diceCount >= maxNumberDice)
        {
            CancelInvoke();
        }

    }

    // Update is called once per frame
    void Update()
    {
        //add another dice to the column if one is removed
        if (diceCount < maxNumberDice)
        {
            MakeSquare();
             
        }


    }
}
