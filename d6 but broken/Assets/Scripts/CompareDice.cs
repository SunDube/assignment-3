﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompareDice : MonoBehaviour
{
    public int newDiceNum;
    public string newColour;
    SpriteRenderer placeholder;
    public Sprite[] purple;
    public Sprite[] blue;
    public Sprite[] pink;
    public Sprite[] darkPink;
    public Sprite[] yellow;
    public Sprite[] green;
    private Sprite onScreen;
    public Sprite blank;

    //combo counter
    GameObject comboCounter;

    //control the reset live
    GameObject lives;

    public AudioClip comboLost;

    private AudioSource mainAuido;

    GameObject speaker;


    // Resets the values every time the program is run
    void Start()
    {
        newDiceNum = 0;
        newColour = null;
        placeholder = GetComponent<SpriteRenderer>();
        onScreen = blank;

        comboCounter = GameObject.FindGameObjectWithTag("Combo");
        lives = GameObject.FindGameObjectWithTag("LifeMan");

        speaker = GameObject.FindGameObjectWithTag("speaker");
        mainAuido = speaker.GetComponent<AudioSource>();
        
    }

    // Update is called once per frame
    void Update()
    {
        FindSprite();
        placeholder.sprite = onScreen;
    }

    //go through the arrays to find the current sprite that should be displayed
    void FindSprite() {
        //find the correct index number
        int currentNum = newDiceNum - 1;

        //find the colour and then pull the sprite using the index number
        if (newColour == "purple") {
            onScreen = purple[currentNum];
        }

        if (newColour == "pink")
        {
            onScreen = pink[currentNum];
        }

        if (newColour == "blue")
        {
            onScreen = blue[currentNum];
        }

        if (newColour == "green")
        {
            onScreen = green[currentNum];
        }

        if (newColour == "yellow")
        {
            onScreen = yellow[currentNum];
        }

        if (newColour == "dark pink")
        {
            onScreen = darkPink[currentNum];
        }

        //sets the sprite to blank if the placeholder has been reset
        if (newColour == null)
        {
            onScreen = blank;
        }



    }

    void OnMouseDown()
    {
        //if statement checks if the dice has already been reset
        if (newDiceNum >= 0) {

            //reset the values of the placeholder dice
            newDiceNum = 0;
            newColour = null;

            //resets the combo when the placeholder is reset
            comboCounter.GetComponent<StreakText>().currentCombo = 0;

            //removes a life when the previous dice is clicked
            lives.GetComponent<LifeIndicator>().lives--;

            //play sound to alert player the streak was lost
            mainAuido.clip = comboLost;
            mainAuido.Play();
        }
    }



}
