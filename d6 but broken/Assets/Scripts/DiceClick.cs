﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceClick : MonoBehaviour
{

    //the instatiator of origin
    GameObject cubeColumn;

    //the number on the dice
    public int diceNum;

    //the colour of the dice
    public string colour;

    //the current dice to be compared to
    GameObject newDice;

    //determines if the the dice can be clicked
    private bool clickable = false;

    //combo counter
    GameObject comboCounter;

    //finds out if loss condition has been met
    GameObject lives;

    private float speed = 0.2f;

    public AudioClip matched;

    public AudioClip notClickable;

    private AudioSource mainAuido;

    GameObject speaker;



    void Update()
    {
        if (lives.GetComponent<LifeIndicator>().lives <0 ) {
            
            if (clickable == true)
            {
                transform.position = Vector2.Lerp(transform.position, transform.position + new Vector3(0, -8.284f), Time.deltaTime * speed);

            }
            else { transform.position = Vector2.Lerp(transform.position, transform.position + new Vector3(0f, -6.568f), Time.deltaTime * speed); }
            
        }
    }
    private void Awake()
    {
        //find the origin column at instantiation
        FindColumn();
        newDice = GameObject.FindGameObjectWithTag("PrevDice");
        comboCounter = GameObject.FindGameObjectWithTag("Combo");
        lives = GameObject.FindGameObjectWithTag("LifeMan");
        speaker = GameObject.FindGameObjectWithTag("speaker");
        mainAuido = speaker.GetComponent<AudioSource>();
        
    }

    //does something when the object is clciked
    void OnMouseDown()
    {
        SetClickable();
        //checks if the dice is in the bottom row
        if (clickable == true)
        {
            //compares the clicked dice to the placekeeper and only destroys it if it meets the criteria
            if (newDice.GetComponent<CompareDice>().newDiceNum + 1 == diceNum || newDice.GetComponent<CompareDice>().newColour == colour)
            {
                //make the appropriate changes
                EditGame();
            }
            else 

            //sets a new placekeeper dice if there is no placekeeper dice set 
            if (newDice.GetComponent<CompareDice>().newDiceNum == 0 || newDice.GetComponent<CompareDice>().newColour == null)
            {
                //make the appropriate changes
                EditGame();
            }

            //makes an exception for the number 6
            if (newDice.GetComponent<CompareDice>().newDiceNum + 1 == 7 && diceNum == 1)
            {
                //make the appropriate changes
                EditGame();
            }

            if (newDice.GetComponent<CompareDice>().newDiceNum + 1 != diceNum && newDice.GetComponent<CompareDice>().newColour != colour) {
                mainAuido.clip = notClickable;
                mainAuido.Play();
            

        }


        }

        if (clickable == false)
        {
            mainAuido.clip = notClickable;
            mainAuido.Play();
        }
    }

    //make changes to the state of the game
    void EditGame() {
        //changes the values of the placekeeper dice
        newDice.GetComponent<CompareDice>().newDiceNum = diceNum;
        newDice.GetComponent<CompareDice>().newColour = colour;
        //removes the clicked dice
        Destroy(this.gameObject);
        //decreases the dice count for the origin instantiator so that a new dice can be added
        cubeColumn.GetComponent<DiceInstantiator>().diceCount--;
        //adds to the combo
        comboCounter.GetComponent<StreakText>().currentCombo++;
        //adds to the score
        comboCounter.GetComponent<StreakText>().currentScore++;

        mainAuido.clip = matched;
        mainAuido.Play();
    

}

    //find the origin instantiate column 
    void FindColumn()
    {
        //Fetch the details of the instantiate column
        GameObject column1 = GameObject.FindGameObjectWithTag("Column1");
        GameObject column2 = GameObject.FindGameObjectWithTag("Column2");
        GameObject column3 = GameObject.FindGameObjectWithTag("Column3");
        GameObject column4 = GameObject.FindGameObjectWithTag("Column4");
        GameObject column5 = GameObject.FindGameObjectWithTag("Column5");

        //compare the x values of the cube and the column to make sure a new cube is put in the correct column
        if (transform.position.x == column1.transform.position.x)
        {
            cubeColumn = column1;

        }

        if (transform.position.x == column2.transform.position.x)
        {
            cubeColumn = column2;

        }

        if (transform.position.x == column3.transform.position.x)
        {
            cubeColumn = column3;

        }

        if (transform.position.x == column4.transform.position.x)
        {
            cubeColumn = column4;

        }

        if (transform.position.x == column5.transform.position.x)
        {
            cubeColumn = column5;

        }

    }

    //find if the dice is in the first row
    void SetClickable() {

        //the dice net that stops the dice from falling offscreen
        GameObject bottomBar = GameObject.FindGameObjectWithTag("Bottom");

        //compares the positon of the dice with the dice net and the radius of the dice
        if (transform.position.y < bottomBar.transform.position.y + 1.8) {
            clickable = true;
        }


    }
}
