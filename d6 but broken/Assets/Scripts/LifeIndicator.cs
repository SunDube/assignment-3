﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeIndicator : MonoBehaviour
{
    //Based on this tutorial https://www.youtube.com/watch?v=3uyolYVsiWc
    public int lives;
    public int numOfDots;
    public Image[] dots;
    public Sprite unused;
    public Sprite used;
    GameObject dice;
    GameObject placeHolder;
    GameObject gameInterf;
    GameObject announce;
    GameObject statDisplay;
    public float speed;

    void Start()
    {
        lives = 6;
        dice = GameObject.FindGameObjectWithTag("Bottom");
        placeHolder = GameObject.FindGameObjectWithTag("PrevDice");
        gameInterf = GameObject.FindGameObjectWithTag("Interface");
        announce = GameObject.FindGameObjectWithTag("WinLose");
        statDisplay = GameObject.FindGameObjectWithTag("stats");
    }

    void Update()
    {
        for (int i = 0; i < dots.Length ; i++)
        {
            if (i < lives)
            {
                dots[i].sprite = unused;
            }
            else {
                dots[i].sprite = used;

            }
        }

        if (lives < 0) {

            dice.transform.position = new Vector2(0, -10);
            placeHolder.transform.position = new Vector3(0, -12);
            gameInterf.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(gameInterf.GetComponent<RectTransform>().anchoredPosition, new Vector2(-1300,0), Time.deltaTime * speed);
            announce.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(announce.GetComponent<RectTransform>().anchoredPosition, new Vector2(0, 0), Time.deltaTime * speed);
            Invoke("PullUpStats",3);
            Invoke("BringUpStats", 3);

        }
    }

    

    void PullUpStats() {
        announce.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(announce.GetComponent<RectTransform>().anchoredPosition, new Vector2(2500, 0), Time.deltaTime * speed);

    }

    void BringUpStats() {
        statDisplay.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(statDisplay.GetComponent<RectTransform>().anchoredPosition, new Vector2(0, 7), Time.deltaTime * speed);

    }
}
